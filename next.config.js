/**
 * 当项目中有 .babelrc 时 nextjs 会退回到 babel 的单文件编译模式
 */

const path = require('path');

module.exports = {
  swcMinify: true, // 使用swc进行压缩
  reactStrictMode: true, // 启用 react 严格模式

  compiler: {
    removeConsole: process.env.NODE_ENV !== 'development', // 删除console.* 方法
  },

  images: {
    domains: ['damon-group.cn'],
    deviceSizes: [1080, 1200, 1920],
  },

  webpack:(config) => {
    config.resolve.alias = {
      ...(config.resolve.alias || Object.create(null)),
      '@common': path.resolve(__dirname, './src/common'),
      '@components': path.resolve(__dirname, './src/components'),
      '@config': path.resolve(__dirname, './src/config'),
      '@type': path.resolve(__dirname, './src/type'),
      '@data': path.resolve(__dirname, './src/data'),
      '@views': path.resolve(__dirname, './src/views'),
      '@styles': path.resolve(__dirname, './src/styles/'),
      '@public': path.resolve(__dirname, './public/'),
      '@assets': path.resolve(__dirname, './src/assets/'),
    };
    return config;
  }
}
