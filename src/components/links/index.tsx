import React from 'react';
import Link, { LinkProps } from 'next/link';
import { Link as MLink, LinkProps as MLinkProps } from '@mui/material';

type LinksType = MLinkProps & LinkProps;

const Links: React.FC<LinksType> = React.forwardRef(
  ({ href, as, children, ...reset },
   ref: React.Ref<HTMLAnchorElement>
  ) => {
    return (
      <Link href={href} as={as} passHref>
        <MLink underline="none" ref={ref} {...reset}>
          {children}
        </MLink>
      </Link>
    )
  })

Links.displayName = 'Links';

export default Links;
