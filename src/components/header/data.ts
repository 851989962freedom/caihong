// import { v4 as uuid } from 'uuid';

export interface NavListTypes {
  id: string;
  name: string;
  url: string;
}

export const navList: Array<NavListTypes>  = [
  {
    name: '产品',
    url: '/product/list/a1',
    id: 'product',
  },
  {
    name: '行业解决方案',
    url: '/industry-solutions',
    id: 'industrySolutions',
  },
  {
    name: '关于彩虹',
    url: '/about-ch',
    id: 'aboutCh',
  },
  {
    name: '服务',
    url: '/service',
    id: 'service',
  },
  {
    name: '联系我们',
    url: '/contact-us',
    id: 'contactUs',
  },
]
