import React from 'react';
import Image from 'next/image';
import Box from '@mui/material/Box';
import Button, { ButtonProps } from '@mui/material/Button';
import Logo0 from '@public/images/logo/0.png';
import Logo1 from '@public/images/logo/1.png';
import Links from '@components/links';
import { styled } from '@mui/material/styles';
import { navList, NavListTypes } from '@components/header/data';
import { useHover,useScroll } from 'ahooks';
import Collapse from '@mui/material/Collapse';
import classNames from 'classnames';
import ProductList from './product-list';
import { MIN_WIDTH } from '@config/index';

type MButtonProps = ButtonProps;

const MButton = styled(({ children, ...reset }: MButtonProps) => (
  <Button {...reset}>{children}</Button>
))(({ theme }) => ({
  color: theme.palette.common.white,
  padding: 0,
  fontSize: theme.typography.body2.fontSize,
  ':hover': {
    color: theme.palette.primary.main + '!important',
    backgroundColor: 'transparent',
    '::before': {
      width: '100%'
    },
  },
  '::before': {
    content: '""',
    position: 'absolute',
    left: '50%',
    bottom: -1,
    transform: 'translateX(-50%)',
    width: 0,
    height: 3,
    background: theme.palette.primary.main,
    transition: `all .36s ${theme.transitions.easing.easeOut}`, // all .36s cubic-bezier(0.785, 0.135, 0.15, 0.86);
  }
}));

const MCollapse = styled(Collapse)(({ theme }) => ({
  '& .ch-Collapse-wrapper': {
    padding: theme.spacing(8.5, 0),
    backgroundColor: theme.palette.neutral.main,
  },
  '& .ch-Collapse-wrapperInner': {
    margin: 'auto',
    width: MIN_WIDTH,
  },
}));

const Item: React.FC<NavListTypes> = (props) => {
  const ref = React.useRef();
  const isHover = useHover(ref);
  return (
    <Box
      ref={ref}
    >
      <Links
        href={props.url}
        underline="none"
        sx={{
          display: 'flex',
          height: 74,
          pl: 5,
        }}
      >
        <MButton>{props.name}</MButton>
      </Links>
      {
        (props.url === '/contact-us' || props.url === '/about-ch')
          ? null
          : (
            <Box
              sx={{
                position: 'absolute',
                left: 0,
                top: '100%',
                width: 1,
              }}
            >
              <MCollapse in={isHover}>
                <ProductList />
              </MCollapse>
            </Box>
          )
      }
    </Box>
  )
}

const Header: React.FC = () => {
  const ref = React.useRef();
  const scroll = useScroll(typeof window !== 'undefined' ? document : null);
  const isHover = useHover(ref);
  return (
    <Box
      ref={ref}
      component="header"
      sx={{
        display: 'flex',
        position: 'relative',
        width: '100%',
      }}
    >
      <Box
        className={classNames({ 'ch-header-active': scroll?.top > 1})}
        sx={{
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          zIndex: 'appBar',
          px: 8,
          width: 1,
          borderBottom: '1px solid rgba(96, 111, 124, .6)',
          transition: 'all .6s',
          ':hover': {
            bgcolor: 'background.paper',
            '& .ch-Button-text': {
              color: 'text.primary',
            },
          },
          '&.ch-header-active': {
            bgcolor: 'background.paper',
            '& > div': {
              width: MIN_WIDTH,
            },
            '& .ch-Button-text': {
              color: 'text.primary',
            },
          },
        }}
      >
        <Box
          sx={{
            width: '100%',
            m: 'auto',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            transition: (theme) => `all .6s ${theme.transitions.easing.sharp}`,
          }}
        >
          <Links href="/">
            <Image width={176} height={38} src={(isHover || scroll?.top > 1) ? Logo1 : Logo0} alt="logo" />
          </Links>
          <Box
            component="nav"
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {
              navList.map((item) => <Item key={item.id} {...item} />)
            }
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default Header;
