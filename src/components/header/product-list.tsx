import React from 'react';
import Box from '@mui/material/Box';
import { product, contentList } from '@data/product';
import Links from '@components/links';

const ProductList: React.FC = () => {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'space-around',
      }}
    >
      {
        product.map((item) => (
          <Box key={item.id}>
            <Links
              href={`/product/list/${item.id}`}
              sx={{
                display: 'block',
                fontSize: 'body1.fontSize',
                fontWeight: 'fontWeightBold',
                marginBottom: 2,
              }}
            >
              {item.name}
            </Links>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
              }}
            >
              {
                contentList[item.id].map((v) => (
                  <Links href={`/product/details/${item.id}`} key={v.id}
                         sx={{
                           color: 'text.secondary',
                           fontSize: 'body2.fontSize',
                           ':hover': {
                             color: 'primary.main',
                           },
                         }}
                  >
                    {v.name}
                  </Links>
                ))
              }
            </Box>
          </Box>
        ))
      }
    </Box>
  )
}

export default ProductList;
