import React from 'react';
import { styled } from '@mui/material/styles';

const Mask = styled('div')(({theme}) => ({
  width: '100%',
  height: 400,
  background: '#141e2c',
}));

export default Mask;
