import React from 'react';
import Router from 'next/router';
import NProgress from 'nprogress';
import Loayouts from '@components/loayout';
import 'nprogress/nprogress.css';

interface IProps {
  pathname?: string;
}

const LoadLoading: React.FC<IProps> = ({ children, pathname }) => {
  const startLoading = (): void => {
    NProgress.start();
  };

  const stopLoading = (): void => {
    NProgress.done();
  };

  const toScrollTop = (): void => {
    if (window) {
      window.scrollTo(0, 0);
    }
  };

  React.useEffect(() => {
    // url 开始改变时触发
    Router.events.on('routeChangeStart', startLoading);
    // url完全改变时触发
    Router.events.on('routeChangeComplete', () => {
      stopLoading();
      toScrollTop();
    });
    // 更改url时发生错误 或 取消url负载时触发
    Router.events.on('routeChangeError', () => {
      stopLoading();
      toScrollTop();
    });

    return (): void => {
      Router.events.off('routeChangeStart', startLoading);
      Router.events.off('routeChangeComplete', stopLoading);
      Router.events.off('routeChangeError', stopLoading);
    };
  }, []);

  return (
    <Loayouts>
      {children}
    </Loayouts>
  );
};

export default LoadLoading;
