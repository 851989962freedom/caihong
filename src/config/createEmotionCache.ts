import createCache from '@emotion/cache';

export default function createEmotionCache() {
  return createCache({ key: 'ch', prepend: true });
}
