import React from 'react';
import { createTheme } from '@mui/material/styles';
import { red, deepOrange } from '@mui/material/colors';
import { TypeText } from '@mui/material/styles';

import { unstable_ClassNameGenerator as ClassNameGenerator } from '@mui/material/utils';

// call this function at the root of the application and before any MUI components import
ClassNameGenerator.configure((componentName) => `ch-${componentName.replace('Mui', '')}`);

interface T extends TypeText {
  white: string;
}

/**
 * 使用 ts 的 模块扩充 来扩展 主题
 */
declare module '@mui/material/styles' {
  interface Theme {
    status: {
      danger: React.CSSProperties['color'];
    };
  }

  interface Palette {
    neutral: Palette['primary'];
  }
  interface PaletteOptions {
    neutral: PaletteOptions['primary'];
  }
}

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: '#BF2722',
    },
    secondary: {
      ...deepOrange,
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
    text: {
      primary: '#333',
      secondary: '#666',
      disabled: '#999',
    },
    background: {
      default: '#f5f5f5',
    },
    // 增加自定义颜色属性
    neutral: {
      main: '#f9f9f9',
    },
  },
  typography: {
    // htmlFontSize: '16px',
    fontFamily: '\'Microsoft Yahei\', \'PingFangSC\', Arial, sans-serif;',
    body1: {
      fontSize: '18px',
    },
    body2: {
      fontSize: '16px',
    },
    subtitle2: {
      fontSize: '12px',
    },
    h1: {
      fontSize: '72px',
    },
    h2: {
      fontSize: '56px',
    },
    h3: {
      fontSize: '36px',
    },
    h4: {
      fontSize: '30px',
    },
    h5: {
      fontSize: '24px',
    },
    h6: {
      fontSize: '20px',
    },
  },
  transitions: {
    easing: {
      sharp: 'cubic-bezier(0.39, 0.575, 0.565, 1)',
    }
  },
});

export default theme;
