/**
 * 服务
 */
import React from 'react';
import Image from 'next/image';
import Box from '@mui/material/Box';

const ServicePage: React.FC = () => {
  return (
    <Box>
      <Image
        width={1920}
        height={500}
        alt="service"
        src="	http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615112330_60c81d32ef7ca.jpg"
      />
    </Box>
  )
}

export default ServicePage;
