/**
 * 产品中心
 */
import React from 'react';
import Box from '@mui/material/Box';
import { useRouter } from 'next/router';
import Mask from '@components/mask';

const ProductDetailsPage: React.FC = () => {
  const router = useRouter();
  return (
    <>
      <Mask />
      <Box>
        产品详情页面: {router.query.id}
      </Box>
    </>
  )
}

export default ProductDetailsPage;
