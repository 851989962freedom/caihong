/**
 * 产品中心
 */
import React from 'react';
import Box from '@mui/material/Box';
import Image from 'next/image';
import { useRouter } from 'next/router'

const ProductListPage: React.FC = () => {
  const router = useRouter()
  return (
    <Box>
      <Image
        alt="fill"
        width={1920}
        height={300}
        src="http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615105034_60c8157a9ebb7.jpg"
      />
      <Box>
        产品列表页面: {router.query.id}
      </Box>
    </Box>
  )
}

export default ProductListPage;
