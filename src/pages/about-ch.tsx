/**
 * 关于彩虹
 */
import React from 'react';
import Image from 'next/image';
import Box from '@mui/material/Box';
import { MAX_WIDTH } from '@config/index';

const AboutChPage: React.FC = () => {
  return(
    <Box>
      <Image
        width={MAX_WIDTH}
        height={600}
        alt="about-ch"
        src="http://damon-group.cn/Public/Uploads/uploadfile/images/20200803/20200803195018_5f27f9fa242ed.jpg"
      />
    </Box>
  )
}

export default AboutChPage;
