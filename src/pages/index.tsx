import React from 'react';
import Home from '@views/home';

const HomePage: React.FC = () => {
  return <Home />
}

export default HomePage;
