/**
 * 行业解决方案
 */
import React from 'react';
import Image from 'next/image';
import Box from '@mui/material/Box';

const IndustrySolutionsPage: React.FC = () => (
  <Box>
    <Image
      width={1920}
      height={500}
      src="http://damon-group.cn/Public/Uploads/uploadfile/images/20200803/20200803144356_5f27b22c344bd.jpg"
      alt="IndustrySolutionsPage"
    />
  </Box>
)

export default IndustrySolutionsPage;
