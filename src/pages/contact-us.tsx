/**
 * 联系我们
 */
import React from 'react';
import Image from 'next/image';
import Box from '@mui/material/Box';

const ContactUsPage: React.FC = () => {
  return (
    <Box>
      <Image
        width={1920}
        height={500}
        alt="ContactUsPage"
        src="http://damon-group.cn/Public/Uploads/uploadfile/images/20200804/20200804112152_5f28d45021f62.jpg"
      />
    </Box>
  )
}

export default ContactUsPage;
