import { v4 as uuid } from 'uuid';

export interface ProductTitleTypes {
  id: string;
  name: string;
}

export interface ProductListTypes {
  id: string;
  name: string;
  title: string;
  img: string;
}

export interface ProductPicTypes {
  id: string;
  img: string;
}

export interface ProductFeatureTypes {
  id: string;
  name: string;
  title: string;
}

export interface ProductDeviceTypes {
  id: string;
  name: string;
  img: string;
}

export interface ProductDetailsTypes {
  id: string;
  title: string;
  introduction: string;
  pic: Array<ProductPicTypes>;
  feature: Array<ProductFeatureTypes>;
  productList: Array<ProductDeviceTypes>;
}

/**
 * 产品标题分类
 */
export const product: Array<ProductTitleTypes> = [
  {
    id: '0',
    name: '智能分拣系统',
  },
  {
    id: '1',
    name: '自动输送系统',
  },
  {
    id: '2',
    name: '智能包装系统',
  },
  {
    id: '3',
    name: '智能制造',
  },
]

/**
 * 产品分类
 */
export const contentList: Array<Array<ProductListTypes>> = [
  [
    {
      id: '1-1',
      name: '摆轮分拣机',
      title: 'i-G5®是全新的第五代模块化智能箱式输送平台',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210220/20210220104254_6030772e3173c.jpg',
    },
    {
      id: '1-2',
      name: '窄带分拣机',
      title: '模块化智能托盘输送平台',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210330/20210330154819_6062d7c3b22a4.png',
    },
    {
      id: '1-3',
      name: 'DWS动态称重系统',
      title: '',
      img: '',
    },
    {
      id: '1-4',
      name: '工业视觉系统',
      title: '',
      img: '',
    },
  ],
  [
    {
      id: '2-1',
      name: '皮带输送机',
      title: '专为快递行业打造的全新第三代模块化皮带输送系统',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615175854_60c879de4fb9c.png',
    },
    {
      id: '2-2',
      name: '滚筒输送机',
      title: '快递输送系统中不可缺少的灵活、可靠的功能模块',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615181751_60c87e4fd4efd.jpg',
    },
    {
      id: '2-3',
      name: '转弯输送机',
      title: '钢平台，能使库房可用面积倍增的“杀手锏”',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615191336_60c88b606e0fb.JPG',
    },
    {
      id: '2-4',
      name: '链板输送机',
      title: '',
      img: '',
    },
  ],
  [
    {
      id: '3-1',
      name: '垂直打带系列',
      title: 'i-G5®是全新的第五代模块化智能箱式输送平台',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210220/20210220104254_6030772e3173c.jpg',
    },
    {
      id: '3-2',
      name: '水平打带系列',
      title: '模块化智能托盘输送平台',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210330/20210330154819_6062d7c3b22a4.png',
    },
    {
      id: '3-3',
      name: '封箱系列',
      title: '',
      img: '',
    },
    {
      id: '3-4',
      name: '贴标系列',
      title: '',
      img: '',
    },
  ],
  [
    {
      id: '4-1',
      name: '皮带输送系列',
      title: '专为快递行业打造的全新第三代模块化皮带输送系统',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615175854_60c879de4fb9c.png',
    },
    {
      id: '4-2',
      name: '辊筒输送系列',
      title: '快递输送系统中不可缺少的灵活、可靠的功能模块',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615181751_60c87e4fd4efd.jpg',
    },
    {
      id: '4-3',
      name: 'DWS系列',
      title: '钢平台，能使库房可用面积倍增的“杀手锏”',
      img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210615/20210615191336_60c88b606e0fb.JPG',
    },
    {
      id: '4-4',
      name: '滑槽系列',
      title: '',
      img: '',
    },
    {
      id: '4-5',
      name: '平台系列',
      title: '',
      img: '',
    },
  ],
]

/**
 * 产品详情数据
 */
export const productDetails: { [key in string]: ProductDetailsTypes } = {
  '1-1': {
    id: uuid(),
    title: '全新第五代模块化智能箱式输送平台',
    introduction: '德马全新的第五代模块化智能输送机平台i-G5®，是一套标准化、柔性化、智能化的适合箱、盒、包裹类物品的输送系统。i-G5®模块化智能输送平台包含了辊筒输送、皮带输送两大系列，任意系列的任意机型都能相互组合。i-G5®平台下的输送系列产品全部采用统一的通用型侧架，整个系列输送机均采用标准化、模块化设计，可任意实现爬坡、转弯、变向、接驳等各种场景的组合应用，也可让您在原有系统中“更换”新的模块或设备单元，让您能够像搭积木一样轻松搭建、配置您的智能输送系统。',
    pic: [
      {
        id: uuid(),
        img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210119/20210119142117_60067a5dc5711.jpg',
      },
      {
        id: uuid(),
        img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210119/20210119142549_60067b6d47f4c.jpg',
      }
    ],
    productList: [
      {
        id: uuid(),
        name: '无动力辊筒输送机',
        img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210610/20210610171510_60c1d81e15080.png',
      },
      {
        id: uuid(),
        name: '托辊式中区皮带机',
        img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210610/20210610171544_60c1d8409af5a.png',
      },
      {
        id: uuid(),
        name: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210610/20210610171605_60c1d855d8dad.png',
        img: '上/下坡托板式中区皮带机',
      },
      {
        id: uuid(),
        name: '窄带汇流机',
        img: 'http://damon-group.cn/Public/Uploads/uploadfile/images/20210610/20210610171644_60c1d87cf2784.png',
      }
    ],
    feature: [
      {
        id: uuid(),
        name: '智',
        title: 'i-G5®是智能物流系统中基于数据采集、智能驱动、EZQube智能控制器/DSmart智能启动器、WCS的“智慧单元”；结合天玑系统（PHM）提供各项智能服务，让输送机与输送系统保持“最佳状态”。',
      },
      {
        id: uuid(),
        name: '捷',
        title: 'i-G5®输送速度120m/min；比上一代产品安装效率提升38%、运维时间节省50%、故障排除时间节省80%、交付时长缩短近一倍；这些均得益于产品标准化设计、i-Selection智选系统与天玑系统（PHM）的运用。',
      },
      {
        id: uuid(),
        name: '绿',
        title: 'i-G5®全系列采用低噪音结构设计，搭配节能型AC电辊筒/AC微电机及EZQube智能驱动控制器/DSmart智能启动器；比上一代输送机噪音降低了6分贝，能耗降低了25%；使得i-G5®平台产品更“绿色”。',
      },
      {
        id: uuid(),
        name: '美',
        title: '高品质的工业设计，小到光感基座、护栏等零件，大到一台整机、一个输送系统乃至整体配色，每个细节都做到极致，德马都将之视为工艺品来打造，让i-G5®平台产品更”招人喜欢”。',
      }
    ]
  },
}
