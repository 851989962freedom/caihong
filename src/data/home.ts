import bg1 from '/public/images/home/module-2/bg1.jpeg';
import bg2 from '/public/images/home/module-2/bg2.jpeg';
import bg3 from '/public/images/home/module-2/bg3.jpeg';
import bg4 from '/public/images/home/module-2/bg4.jpeg';
import icon1 from '/public/images/home/module-2/1.png';
import icon2 from '/public/images/home/module-2/2.png';
import icon3 from '/public/images/home/module-2/3.png';
import icon4 from '/public/images/home/module-2/4.png';

export const homeData = {
  module2: {
    title: '彩虹科技',
    introduction: '智能物流系统及核心装备提供商',
    list: [
      {
        id: 0,
        title: '智能输送系统',
        bg: bg1,
        icon: icon1,
        content: '德马输送系统解决方案是由箱式输送、托盘输送、垂直输送、皮带式输送、悬挂链等多种类型机型构成...',
      },
      {
        id: 1,
        title: '智能分拣系统',
        bg: bg2,
        icon: icon2,
        content: '德马输送系统解决方案是由箱式输送、托盘输送、垂直输送、皮带式输送、悬挂链等多种类型机型构成...',
      },
      {
        id: 2,
        title: '密集存取系统',
        bg: bg3,
        icon: icon3,
        content: '德马输送系统解决方案是由箱式输送、托盘输送、垂直输送、皮带式输送、悬挂链等多种类型机型构成...',
      },
      {
        id: 3,
        title: '智能搬运系统',
        bg: bg4,
        icon: icon4,
        content: '德马输送系统解决方案是由箱式输送、托盘输送、垂直输送、皮带式输送、悬挂链等多种类型机型构成...',
      },
    ],
  },
}
