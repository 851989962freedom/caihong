import Carousel from './components/carousel';
import CHong from './components/caihong';

function Home() {
  return (
    <>
      <Carousel />
      <CHong />
    </>
  )
}

export default Home;
