/** @jsxImportSource @emotion/react */
import React from 'react';
import Image from 'next/image';
import Box, { BoxProps } from '@mui/material/Box';
import { EffectFade, Pagination, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { v4 as uuid } from 'uuid';
import { styled } from '@mui/material/styles';
import classNames from 'classnames';

import styles from '@styles/home/swiper.module.css';

import brander1 from '/public/images/home/module-1/01.webp';
import brander2 from '/public/images/home/module-1/02.webp';
import brander3 from '/public/images/home/module-1/03.webp';
import brander4 from '/public/images/home/module-1/04.webp';

const data = [
  {
    id: uuid(),
    src: brander1,
    title: 'hello word'
  },
  {
    id: uuid(),
    src: brander2,
    title: '欢迎来到彩虹科技'
  },
  {
    id: uuid(),
    src: brander3,
    title: 'freedom.yi'
  },
  {
    id: uuid(),
    src: brander4,
    title: '你好！全世界'
  }
]

type BoxType = BoxProps & { isActive: boolean; title: string };


// 做动画建议使用这种方式
const Item = styled(({ isActive, title, className, ...reset }: BoxType) => (
  <Box className={classNames(className,{ 'active': isActive })} {...reset}>{title}</Box>
))(({ theme }) => `
  font-weight: ${theme.typography.fontWeightBold};
  color: ${theme.palette.common.white};
  font-size: ${theme.typography.h1.fontSize}; 
  transition: all 1.5s .3s;
  transform: scale(1.3);
  text-shadow: 0 10px 40px rgb(0 0 0 / 48%);
  filter: blur(10px);
  &.active {
    filter: blur(0);
    transform: scale(1);
  }
`)

const Carousel: React.FC = () => {
  const pagination = {
    clickable: true,
    renderBullet: function (index, className) {
      return `<span class="${className}">${index + 1}</span>`;
    },
  };

  return (
    <Swiper
      effect="fade"
      spaceBetween={50}
      slidesPerView={1}
      pagination={pagination}
      autoplay={{ delay: 3000 }}
      modules={[Autoplay, EffectFade, Pagination]}
    >
      {
        data.map((item) => (
          <SwiperSlide key={item.id} className={styles.slider}>
            {({ isActive }) => (
              <>
                <Image src={item.src} alt="brander1" placeholder="blur" />
                <Box
                  sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    zIndex: 'tooltip',
                    transform: 'translate(-50%, -50%)'
                  }}
                >
                  <Item title={item.title} isActive={isActive} />
                </Box>
              </>
            )}
          </SwiperSlide>
        ))
      }
    </Swiper>
  )
}

export default Carousel;
