import React from 'react';
import Box from '@mui/material/Box';
import { homeData } from '@data/home';
import Links from '@components/links';
import Image from 'next/image';
import { MIN_WIDTH } from '@config/index';

const CHong: React.FC = () => {
  return (
    <Box
      sx={{
        m: 'auto',
        width: MIN_WIDTH,
        pt: 7.5,
        pb: 12.5,
        overflow: 'hidden',
      }}
    >
      <Box
        sx={{
          textAlign: 'center',
          fontSize: 'h3.fontSize',
        }}
      >
        {homeData.module2.title}
      </Box>
      <Box
        sx={{
          textAlign: 'center',
          fontSize: 'h4.fontSize',
        }}
      >
        {homeData.module2.introduction}
      </Box>
      <Box
        sx={{
          mt: 12.5,
          display: 'flex',
          mr: -18.75,
        }}
      >
        {
          homeData.module2.list.map((item, index) => (
            <Links
              href={`/product/list/${item.id}`}
              key={item.id}
              sx={{
                width: 337,
                display: 'flex',
                bgcolor: '#384656',
                position: 'relative',
                color: 'common.white',
                transform: `translate(-${index * 50}px, ${index * 25}px)`,
                transition: (theme) => `all .36s ${theme.transitions.easing.easeOut}`,
                ':hover': {
                  transform: `translate(-${index * 50}px, 0)`,
                  '& > span > img': {
                    opacity: 0,
                  },
                },
                '& > span > img': {
                  transition: (theme) => `all .36s ${theme.transitions.easing.easeOut}`,
                },
              }}
            >
              <Image src={item.bg} alt="bg" />
              <Box
                sx={{
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  width: 1,
                  height: 1,
                  zIndex: 2,
                  padding: (theme) => theme.spacing(5.65, 11.25, 4.4, 3.75),
                }}
              >
                <Box
                  component="h4"
                  sx={{
                    lineHeight: '54px',
                    fontSize: 'h5.fontSize',
                    fontWeight: 500,
                    margin: (theme) => theme.spacing(0.5, 0, 1.5),
                  }}
                >
                  {item.title}
                </Box>
                <Box
                  component="p"
                  sx={{
                    fontSize: 14,
                  }}
                >
                  {item.content}
                </Box>
                <Box
                  sx={{
                    position: 'absolute',
                    left: 30,
                    bottom: 35,
                  }}
                >
                  <Image src={item.icon} alt="icon" />
                </Box>
              </Box>
            </Links>
          ))
        }
      </Box>
    </Box>
  )
}

export default CHong;
